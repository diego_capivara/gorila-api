# Instalação

```
$ npm install
```

# Executar projeto apontando para base local

```
$ npm run start:debug
```

# Executar projeto apontando para base de produção

```
$ npm run start
```

# Buildar projeto em modo de produção

```
$ npm run start:prod
```

# Link Heroku
https://gorila-invest-api.herokuapp.com/