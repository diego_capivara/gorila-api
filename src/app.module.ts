import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseHandlerModule } from './mongoose-handler/mongoose-handler.module';
import { EnvironmentModule } from './environment/environment.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { InvestmentsModule } from './investments/investments.module';

@Module({
  imports: [
    MongooseHandlerModule, 
    EnvironmentModule, 
    AuthModule, 
    UsersModule, 
    InvestmentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
