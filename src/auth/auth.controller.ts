import { Controller, Post, Req, Body, UnauthorizedException, Put, BadRequestException } from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

    constructor(
        private authService: AuthService,
    ) { }

    /**
     * 
     * @param req 
     * @param body
     * email: string
     * password: string 
     */
    @Post('login')
    public async loginJwt(@Req() req, @Body() body) {
        const payload = body;
        const user = await this.authService.validateUser(payload.email, payload.password);
        if (!user) {
            throw new UnauthorizedException();
        }
        return await this.authService.generateJWT(user);
    }

    /**
     * 
     * @param req 
     * @param body 
     * authorization: -> Bearer Token
     */
    @Put('refresh-token')
    public async refreshToken(@Req() req, @Body() body) {
        const jwtToken = body.authorization;
        if (!jwtToken) {
            return new BadRequestException('Empty Token');
        }
        const decodeToken: any = await this.authService.parseJWT(jwtToken);
        const user = await this.authService.verify({ _id: decodeToken.id });
        if (Object.keys(user).length === 0 || !user._id) {
            return new BadRequestException('Invalid token');
        }
        return await this.authService.generateJWT(user);
    }
}
