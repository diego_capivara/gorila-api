import { EnvironmentService } from "src/environment/environment.service";
import { Environment } from "src/environment/model/env.model";

const environmentService = new EnvironmentService();
const env: Environment = environmentService.env();
const secret = env.JWT_SECRET;

export const jwtConstants = {
    secret,
};