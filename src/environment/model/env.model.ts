export interface Environment {
    MONGO_LINK: string,
    JWT_SECRET: string,
    type: string,
}