import { Controller, Request, Post, Body, UseGuards, Get, Query, Delete, Param, BadRequestException } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt/jwt-auth.guard';
import { InvestmentsService } from './investments.service';

@Controller('investments')
export class InvestmentsController {

    constructor(
        private investimentService: InvestmentsService
    ) {}

    @UseGuards(JwtAuthGuard)
    @Post()
    public async create(@Request() req, @Body() body) {
        const params = body;
        return await this.investimentService.create(params);
    }

    @UseGuards(JwtAuthGuard)
    @Get('search')
    public async search(@Request() req, @Query() query) {
        if (Object.keys(query).length === 0) {
            return new BadRequestException('Empty query');
        }
        return await this.investimentService.search(query);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    public async getAll(@Request() req, @Body() body, @Query() query) {
        const params = query;
        return await this.investimentService.get(params);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/:id')
    public async findOne(@Request() req, @Param() params) {
        return await this.investimentService.findByID(params.id);
    }

    @UseGuards(JwtAuthGuard)
    @Delete('/:id')
    public async delete(@Request() req, @Param() params) {
        return await this.investimentService.deleteByID(params.id);
    }
}
