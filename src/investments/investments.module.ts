import { Module } from '@nestjs/common';
import { InvestmentsController } from './investments.controller';
import { InvestmentsService } from './investments.service';
import { MongooseModule } from '@nestjs/mongoose';
import { investmentsSchema } from './schema/investments.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Investments', schema: investmentsSchema }]),
  ],
  controllers: [InvestmentsController],
  providers: [InvestmentsService],
  exports: [InvestmentsService],
})
export class InvestmentsModule {}
