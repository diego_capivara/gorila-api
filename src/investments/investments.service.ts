import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { InvestimentModel } from './model/investments.model';
import * as moment from 'moment';

@Injectable()
export class InvestmentsService {
    constructor(
        @InjectModel('Investments') private readonly investimentModel: Model<InvestimentModel>,
    ) { }

    public async findOne(params?, id?: string): Promise<any> {
        return await this.investimentModel.findOne(params ? params : id)
            .then(Investiment => {
                return (Investiment)
                    ? Promise.resolve(Investiment)
                    : Promise.reject('Investiment not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async findByID(id: string): Promise<any> {
        return await this.investimentModel.findById(id)
            .then(Investiment => {
                return (Investiment)
                    ? Promise.resolve(Investiment)
                    : Promise.reject('Investiment not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async search(query: any = {}): Promise<any> {
        return await this.investimentModel.find(query)
            .then(Investiment => {
                return (Investiment)
                    ? Promise.resolve(Investiment)
                    : Promise.reject('Investiment not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async deleteByID(id: string): Promise<any> {
        return await this.investimentModel.findByIdAndRemove(id)
            .then(Investiment => {
                return (Investiment)
                    ? Promise.resolve(Investiment)
                    : Promise.reject('Investiment not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async get(params): Promise<any> {
        return await this.investimentModel.find(params)
            .then(Investiment => {
                return (Investiment)
                    ? Promise.resolve(Investiment)
                    : Promise.reject('Investiment not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async create(params): Promise<any> {
        params.date = moment(params.date, 'DD/MM/YYYY').toISOString();
        return await this.investimentModel.create(params)
            .then(Investiment => {
                return (Investiment)
                    ? Promise.resolve(Investiment)
                    : Promise.reject('Investiment not exist');
            }, (err) => {
                return Promise.reject(new BadRequestException(err));
            }).catch(err => Promise.reject(new NotFoundException(err)));
    }
}
