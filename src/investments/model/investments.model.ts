export interface InvestimentModel {
    type: String,
    value: String,
    createdAt: Date,
    updatedAt: Date,
}