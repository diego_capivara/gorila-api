import * as mongoose from 'mongoose';
const mongoose = require('mongoose');

const schema = {
    type: String,
    value: Number,
    date: { type: Date, default: Date.now },

    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
};

export const investmentsSchema = new mongoose.Schema(schema,
  {
    collection: 'investments',
    read: 'nearest',
  },
);
