export interface UserModel {
    username: String,
    password: String,
    email: String,
    createdAt: Date,
    updatedAt: Date
}