import * as mongoose from 'mongoose';
const mongoose = require('mongoose');

const schema = {
    username: String,
    password: { type: String },
    email: { type: String },


    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
};

export const userSchema = new mongoose.Schema(schema,
  {
    collection: 'users',
    read: 'nearest',
  },
);
