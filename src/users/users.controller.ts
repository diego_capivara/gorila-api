import { Controller, Request, Post, Body, Query, Get, Param, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from 'src/auth/jwt/jwt-auth.guard';

@Controller('users')
export class UsersController {
    constructor(
        private usersService: UsersService,
    ) {}

    @Post()
    public async create(@Request() req, @Body() body) {
        const params = body;
        return await this.usersService.create(params);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    public async getAll(@Request() req, @Body() body, @Query() query) {
        const params = query;
        return await this.usersService.get(params);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/:id')
    public async findOne(@Request() req, @Param() param) {
        const params = param;
        return await this.usersService.findByID(params.id);
    }
}
