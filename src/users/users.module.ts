import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { userSchema } from './schema/user.schema';
import { UsersController } from './users.controller';
import { CryptService } from 'src/auth/crypt/crypt.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Users', schema: userSchema }]),
  ],
  providers: [UsersService, CryptService],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
