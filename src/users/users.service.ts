import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserModel } from './model/users.model';
import { Model } from 'mongoose';
import { CryptService } from 'src/auth/crypt/crypt.service';

@Injectable()
export class UsersService {

    constructor(
        @InjectModel('Users') private readonly User: Model<UserModel>,
        private cryptService: CryptService,
    ) { }

    public async findOne(params?, id?: string): Promise<any> {
        return await this.User.findOne(params ? params : id)
            .then(user => {
                return (user)
                    ? Promise.resolve(user)
                    : Promise.reject('User not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async findByID(id: string): Promise<any> {
        console.log('id', id);
        return await this.User.findById(id)
            .then(user => {
                return (user)
                    ? Promise.resolve(user)
                    : Promise.reject('User not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async get(params): Promise<any> {
        return await this.User.find(params)
            .then(user => {
                return (user)
                    ? Promise.resolve(user)
                    : Promise.reject('User not exist');
            })
            .catch(err => Promise.reject(new NotFoundException(err)));
    }

    public async create(params): Promise<any> {
        params.password = this.cryptService.hashPassword(params.password);
        return await this.User.create(params)
            .then(user => {
                return (user)
                    ? Promise.resolve(user)
                    : Promise.reject('User not exist');
            }, (err) => {
                return Promise.reject(new BadRequestException(err));
            }).catch(err => Promise.reject(new NotFoundException(err)));
    }
}
